using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Text.Json;
using Newtonsoft.Json;

namespace ConsoleApp2
{
    class Test
    {
        int field;

        public Test(int field)
        {
            this.field = field;
            //Console.WriteLine($"Constructor {field}");
        }
    }

    class Logger : IDisposable
    {
        public FileStream FileStream { get; set; }
        public StreamWriter StreamWriter { get; set; }

        public Logger(string filename)
        {
            FileStream = new FileStream(filename, FileMode.Append);
            StreamWriter = new StreamWriter(FileStream);
        }

        public void Log(string text)
        {
            StreamWriter.WriteLine($"{DateTime.Now} - {text}");
        }

        bool isClosed = false;

        public void Close()
        {
            Dispose();
        }

        public void Dispose()
        {
            if (!isClosed)
            {
                StreamWriter.Close();
                isClosed = true;
                GC.SuppressFinalize(this);
            }
        }

        ~Logger()
        {
            if (!isClosed)
            {
                StreamWriter.Close();
            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(GC.GetTotalMemory(false));
            List<int[]> list = new List<int[]>();

            while (true)
            {
                int[] arr = new int[10000];
                list.Add(arr);
                Console.WriteLine(GC.GetTotalMemory(false));
            }



            //using (var logger = new Logger("log.txt"))
            //{
            //    logger.Log("Hello!");
            //    logger.Log("Test log...");
            //}




            //var logger = new Logger("log.txt");
            //logger.Log("Hello!");
            //logger.Log("Test log...");
            //logger.Close();






            //using (var fs = new FileStream("test.txt", FileMode.Open))
            //{
            //    fs.Write(...)
            //}


            //FileStream fs = null;
            //try
            //{
            //    fs = new FileStream("test.txt", FileMode.Open);
            //    fs.Write(...);
            //}
            //finally
            //{
            //    fs.Dispose();
            //}
        }
    }
}
