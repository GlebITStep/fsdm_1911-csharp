using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;

namespace NewConsoleApp
{
    class Person
    {
        private int age;

        public Person(string name = "Empty", string surname = "Empty", int age = 0)
        {
            Name = name;
            Surname = surname;
            Age = age;
        }

        public override string ToString() => $"{Name} {Surname} {Age}";

        public string Name { get; set; }

        public string Surname { get; set; }

        public int Age
        {
            get { return age; }
            set
            {
                if (value > 0 && value <= 100)
                    age = value;
                else
                    age = 0;
            }
        }

    }

    class Program
    {
        static void Main(string[] args)
        {
            //Console.ForegroundColor = ConsoleColor.DarkMagenta;
            //Console.BackgroundColor = ConsoleColor.White;
            //Console.SetCursorPosition(5, 5);

            ////system("cls")
            //Console.Clear();

            //Console.Beep();

            ////Sleep(1000)
            //Thread.Sleep(1000);

            ////system("pause")
            //Console.WriteLine("Press any key to continue...");
            //Console.ReadKey();



            //File.Encrypt("people.txt");
            //Environment.Exit(0);



            //List<Person> people = new List<Person>()
            //{
            //    new Person { Name = "Gleb", Surname = "Skripnikov", Age = 25 },
            //    new Person { Name = "One", Surname = "Two", Age = 3 },
            //    new Person { Name = "Four", Surname = "Five", Age = 6 },
            //};
            //StringBuilder text = new StringBuilder();
            //foreach (var item in people)
            //{
            //    text.Append($"{item}\n");
            //}
            //File.WriteAllText("people.txt", text.ToString());

            //List<Person> people = new List<Person>();
            //string[] lines = File.ReadAllLines("people.txt");
            //foreach (var line in lines)
            //{
            //    string[] parts = line.Split(' ');
            //    Person person = new Person
            //    {
            //        Name = parts[0],
            //        Surname = parts[1],
            //        Age = int.Parse(parts[2])
            //    };
            //    people.Add(person);
            //}

            //foreach (var item in people)
            //{
            //    Console.WriteLine(item);
            //}





            //Person person = new Person 
            //{ 
            //    Name = "Gleb", 
            //    Surname = "Skripnikov", 
            //    Age = 25 
            //};
            //File.WriteAllText("person.txt", person.ToString());

            //string data = File.ReadAllText("person.txt");
            //string[] parts = data.Split(' ');
            //Person person = new Person
            //{
            //    Name = parts[0],
            //    Surname = parts[1],
            //    Age = int.Parse(parts[2])
            //};
            //Console.WriteLine(person.Surname);




            //File.WriteAllText("text.txt", "Hello, world!");

            //string text = File.ReadAllText("text.txt");
            //Console.WriteLine(text);
        }
    }
}