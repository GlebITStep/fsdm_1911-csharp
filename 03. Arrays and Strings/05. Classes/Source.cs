using System;
using System.Text;

namespace NewConsoleApp
{
    class Student
    {
        private double average;
        public double Average
        {
            get => average;
            set => average = value >= 0 && value <= 12 ? value : 0;
        }



        //private double average;
        //public double Average 
        //{ 
        //    get => average;
        //    set
        //    {
        //        if (value >= 0 && value <= 12)
        //            average = value;
        //        else
        //            average = 0;
        //    }
        //}



        public string Name { get; set; }
        public string Surname { get; set; }
    }

    class Person
    {
        const int test = 5;


        //static int count = 0;
        static public int Count { get; private set; }


        //public readonly int id;
        public int Id { get; } 


        //prop
        public string Name { get; set; } = "Empty"; //Auto-property


        //propfull
        private string surname = "Empty";
        public string Surname
        {
            get { return surname; }
            set { surname = value; }
        }


        private int age;
        public int Age //Proprety
        {
            get { return age; }
            set 
            {
                if (value >= 0 && value <= 100)
                    age = value;
                else
                    age = 0;
            }
        }

        static Person()
        {
            Count = 0;
            Console.WriteLine("I'm a static constructor!");
        }

        public Person()
        {
            Count++;
            Console.WriteLine($"Person #{Count} created!");
        }

        public Person(string name, int age) : this()
        {
            Id = Count;
            Name = name;
            Age = age;
        }
    }

    class Program
    {
        static void SumPrint(int x, int y) => Console.WriteLine(x + y);
        static int Sum(int x, int y) => x + y;

        static void Main(string[] args)
        {
            Console.WriteLine(Person.Count);

            //Person person = new Person("Gleb", 25);
            //person.Age = 26;
            //person.Name = "Qleb";
            //Console.WriteLine($"Id: {person.Id}");
            //Console.WriteLine($"Name: {person.Name}");
            //Console.WriteLine($"Age: {person.Age}");


            //Person person2 = new Person("Igor", 33);
            //Console.WriteLine($"Id: {person2.Id}");
            //Console.WriteLine($"Name: {person2.Name}");
            //Console.WriteLine($"Age: {person2.Age}");

            Console.WriteLine(Person.Count);
        }
    }
}