using System;
using System.Text;
using NewConsoleApp.Gleb;


namespace NewConsoleApp
{
    namespace Gleb
    {
        //class StringBuilder
        //{ 
        
        //}

        struct Point
        {
            public int x;
            public int y;
        }

        class Person
        {
            public int age;
            public string name;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            //int[] arr = null;
            //int[] arr = { 1, 2, 3, 4, 5 };
            //int[] arr = new int[5];
            //int[] arr = new int[5] { 1, 2, 3, 4, 5 };





            //int[] arr = { 1, 2, 3, 4, 5 };
            //for (int i = 0; i < arr.Length; i++)
            //{
            //    Console.WriteLine(arr[i]);
            //}





            //int[] arr = { 1, 2, 3, 4, 5 };
            //foreach (var item in arr)
            //{
            //    Console.WriteLine(item);
            //}




            //int[,] arr = 
            //{ 
            //    { 1, 2, 3 },
            //    { 4, 5, 6 },
            //    { 7, 8, 9 },
            //    { 10, 11, 12 }
            //};

            ////foreach (var item in arr)
            ////{
            ////    Console.WriteLine(item);
            ////}

            //Console.WriteLine(arr.Length);
            //for (int i = 0; i < arr.GetLength(0); i++)
            //{
            //    for (int j = 0; j < arr.GetLength(1); j++)
            //    {
            //        Console.Write($"{arr[i, j]}\t");
            //    }
            //    Console.WriteLine();
            //}





            //int[][] arr =
            //{
            //    new int [] { 1, 2, 3 },
            //    new int [] { 4, 5 },
            //    new int [] { 6, 7, 8, 9 }
            //};

            //foreach (var subArr in arr)
            //{
            //    foreach (var item in subArr)
            //    {
            //        Console.Write($"{item}\t");
            //    }
            //    Console.WriteLine();
            //}

            ////for (int i = 0; i < arr.Length; i++)
            ////{
            ////    for (int j = 0; j < arr[i].Length; j++)
            ////    {
            ////        Console.Write($"{arr[i][j]}\t");
            ////    }
            ////    Console.WriteLine();
            ////}




            //int[] arr = { 7, 5, 1, 9, 2 };
            ////Array.Reverse(arr);
            //Array.Sort(arr);
            //int index = Array.BinarySearch(arr, 5);
            //Console.WriteLine(index);
            //foreach (var item in arr)
            //{
            //    Console.WriteLine(item);
            //}




            //string str = "Hello";
            ////Console.WriteLine(str[0]);
            //Console.WriteLine($"{str} - Length: {str.Length}");
            //str += "!!!"; //str = str + "!!!";
            //str = str.ToUpper();
            //Person p = new Person();
            //Console.WriteLine(str);



            //string str = "";
            //for (int i = 0; i < 10; i++)
            //{
            //    str += i;
            //}
            //Console.WriteLine(str);



            //StringBuilder strBuilder = new StringBuilder(256);
            //Console.WriteLine(strBuilder.Capacity);
            //for (int i = 0; i < 100; i++)
            //{
            //    Console.WriteLine(strBuilder.Capacity);
            //    strBuilder.Append(i);
            //}
            //Console.WriteLine(strBuilder);
            //string str = strBuilder.ToString();  



            string str = "Hello!";
            //Console.WriteLine(str.IndexOf('e'));
            //Console.WriteLine(str.IndexOfAny(new char[]{ '!', '?', '.' }));
            //Console.WriteLine(str.IndexOf('l'));
            //Console.WriteLine(str.LastIndexOf('l'));

            //Console.WriteLine(str.Contains("Hell"));

            //Console.WriteLine(str.Trim());

            //str = str.Insert(2, "123");
            //Console.WriteLine(str);

            //str = str.Remove(str.IndexOf('h'), 2);
            //Console.WriteLine(str);

            //str = str.Replace("l", "");
            //Console.WriteLine(str);

            //string fullname = "Gleb Skripnikov";
            //string name = fullname.Substring(0, fullname.IndexOf(' '));
            //string surname = fullname.Substring(fullname.IndexOf(' ') + 1);
            //Console.WriteLine(name);
            //Console.WriteLine(surname);


            string fullname = "Skripnikov Gleb Alexeevich";
            string[] arr = fullname.Split(' ');
            string name = arr[1];
            string surname = arr[0];
            string middlename = arr[2];
            Console.WriteLine(name);
            Console.WriteLine(surname);
            Console.WriteLine(middlename);
        }
    }
}




// .NET
// Languages: C#, VB.NET, F#
// Desktop: WindowsFroms, WPF, UWP
// Mobile: Xamarin
// WEB Back-End: ASP.NET
// WEB Front-End: Blazor
// Games: Unity

// IL (MSIL) - Intermediate language
// CLR - Common Language Runtime
//      JIT Compiler - Just-In-Time Compiler
//      GC - Garbage Collector
// BCL - Base Class Library

// .NET Framework 4.8 - Only Windows
// .NET Core 3.0 - Cross-Platfotm, Open-Source
// .NET 5

// Reflector
// Dotfuscator