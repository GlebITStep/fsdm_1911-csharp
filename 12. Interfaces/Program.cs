using System;
using System.Collections.Generic;
using System.IO;
using System.Security;
using System.Text;
using System.Threading;
using Zoo;

namespace NewConsoleApp
{
    abstract class Animal
    {
        protected Animal(string name = "Empty", int age = 0)
        {
            Name = name;
            Age = age;
        }

        public string Name { get; set; }
        public int Age { get; set; }

        public abstract void Say();

        public virtual void Info()
        {
            Console.WriteLine($"Name: {Name}");
            Console.WriteLine($"Age: {Age}");
        }

        public override string ToString() => $"{Name} {Age}";
    }

    class Cat : Animal, ISpeaking
    {
        public Cat(string name, int age, int lives) : base(name, age)
        {
            Lives = lives;
        }

        public int Lives { get; set; }

        ///public new void Say()
        public override void Say()
        {
            Console.WriteLine($"{Name} says Meow");
        }

        public override sealed void Info()
        {
            Console.WriteLine("Type: Cat");
            base.Info();
            Console.WriteLine($"Lives: {Lives}");
        }
    }
    class Lion : Cat
    {
        public Lion(string name, int age, int lives) : base(name, age, lives)
        {
        }

        public override void Say()
        {
            Console.WriteLine($"{Name} says Arrr");
        }
    }

    class Dog : Animal
    {
        public double Weight { get; set; }

        public Dog(string name, int age, double weight) : base(name, age)
        {
            Weight = weight;
        }

        public override void Say()
        {
            Console.WriteLine($"{Name} says Woof");
        }

        public override void Info()
        {
            Console.WriteLine("Type: Dog");
            base.Info();
            Console.WriteLine($"Weight: {Weight}");
        }
    }

    class Person : ISpeaking, ICloneable, IComparable
    {
        public Person(string name, string surname, int age)
        {
            Name = name;
            Surname = surname;
            Age = age;
        }

        public string Name { get; set; }
        public string Surname { get; set; }
        public int Age { get; set; }

        //public override bool Equals(object obj)
        //{
        //    Person person = obj as Person;
        //    return Name == person.Name && Surname == person.Surname && Age == person.Age;
        //}

        public object Clone()
        {
            return new Person(Name, Surname, Age);
        }

        //public int CompareTo(object obj)
        //{
        //    Person second = obj as Person;
        //    if (Age > second.Age)
        //        return 1;
        //    else if (Age < second.Age)
        //        return -1;
        //    else
        //        return 0;
        //}

        //public int CompareTo(object obj)
        //{
        //    return Age - (obj as Person).Age;
        //}

        public int CompareTo(object obj)
        {
            return (obj as Person).Name.CompareTo(this.Name) * -1;
        }

        public void Say()
        {
            Console.WriteLine($"{Name} {Surname} says 'Hello!'");
        }

        public override string ToString() => $"{Name} {Surname} {Age}";


        //public Person Clone()
        //{
        //    return new Person(Name, Surname);
        //}
    }

    class Car : ISpeaking, ICloneable
    {
        public string Model { get; set; }

        public object Clone()
        {
            return new Car { Model = Model };
        }

        public void Say()
        {
            Console.WriteLine("Car says Beep");
        }

        //public Car Copy()
        //{
        //    return new Car { Model = Model };
        //}
    }

    interface ISpeaking
    {
        void Say();
    }

    //IClonable
    //IComparable
    //IEnumberable
    //IConvertable
    //IDisposable
    //IEquatable

    interface IAdmin
    {
        void Put();
    }

    interface IPoliceman
    {
        void Put();
    }

    class Employee : IAdmin, IPoliceman
    {
        public string Name { get; set; }

        public void Put()
        {
            Console.WriteLine($"{Name} puts the beer in the frige");
        }

        void IAdmin.Put()
        {
            Console.WriteLine($"{Name} puts mouse in the PC");
        }

        void IPoliceman.Put()
        {
            Console.WriteLine($"{Name} puts someone into jail");
        }
    }


    class Program
    {
        static void SaySomething(ISpeaking speaking)
        {
            Console.WriteLine(speaking);
            speaking.Say();
        }

        static void Main(string[] args)
        {
            Employee employee = new Employee { Name = "Igor" };
            employee.Put();
            IAdmin admin = employee;
            admin.Put();
            IPoliceman policeman = employee;
            policeman.Put();



            //List<Person> people = new List<Person>()
            //{
            //    new Person("Nigar", "Khasaeva", 18),
            //    new Person("Gleb", "Skripnikov", 25),
            //    new Person("Sabik", "Mehdiev", 20),
            //};

            //foreach (var item in people)
            //{
            //    Console.WriteLine(item);
            //}
            //Console.WriteLine();

            //people.Sort();

            //foreach (var item in people)
            //{
            //    Console.WriteLine(item);
            //}
            //Console.WriteLine();



            //Person person1 = new Person("Igor", "Nikolaev");
            //Person person2 = person1.Clone() as Person;

            //Car car1 = new Car { Model = "Tesla Cyber Track" };
            //Car car2 = car1.Clone() as Car;





            //Cat animal = new Cat("Barsik", 3, 9);
            //Person person = new Person("Igor", "Nikolaev");
            //Car car = new Car();

            ////animal.Say();
            ////person.Say();

            //ISpeaking speaking = animal;
            //speaking = person;
            //speaking = car;

            //SaySomething(animal);
            //SaySomething(person);
        }
    }
}