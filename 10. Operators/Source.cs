using System;
using System.Collections.Generic;
using System.IO;
using System.Security;
using System.Text;
using System.Threading;

namespace NewConsoleApp
{
    enum Gender
    {
        Male = 1,
        Female = 2,
        Other = 0
    }

    class Person
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public Gender Gender { get; set; }

        //public override bool Equals(object obj)
        //{
        //    if (obj is Person)
        //    {
        //        Person person = obj as Person;
        //        return this.Name == person.Name && this.Age == person.Age;
        //    }
        //    return false;
        //}

        public override bool Equals(object obj)
        {
            if (obj is Person person)
                return this.Name == person.Name && this.Age == person.Age;            
            return false;
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode() + Age.GetHashCode() + Gender.GetHashCode();
        }

        public override string ToString() => $@"{Name} {Age} {Gender}";
    }

    class Money
    {
        private int cents;
        public int Cents 
        { 
            get => cents;
            set
            {
                if (value >= 100)
                {
                    Dollars += value / 100;
                    cents = value % 100;
                }
                else
                    cents = value;
            }
        }

        public int Dollars { get; set; }

        // ==
        public static bool operator ==(Money left, Money right)
        {
            return left.Dollars == right.Dollars && left.Cents == right.Cents;
        }

        public static bool operator !=(Money left, Money right)
        {
            return !(left == right);
        }

        public static Money operator ++(Money money)
        {
            return new Money { Dollars = money.Dollars, Cents = money.Cents + 1 };
        }

        public static bool operator true(Money money)
        {
            return money.Cents > 0;
        }

        public static bool operator false(Money money)
        {
            return money.Cents <= 0;
        }

        public override string ToString() => $"{Dollars},{Cents}";

    }

    partial class Test
    {
        public int One { get; set; }
    }

    partial class Test
    {
        public int Two { get; set; }
    }

    class Program
    {
        static (int, int) MaxMin(params int[] arr)
        {
            return (10, 1);
        }

        static void Main(string[] args)
        {
            //Test test = new Test();


            //MaxMin(1, 2, 3, 4, 5);


            //var obj = new { Name = "Gleb", Surname = "Skripnikov" };
            //Console.WriteLine($"{obj.Name} {obj.Surname}");


            ////Nullable<bool> answer = false;
            //bool? answer = null;
            //if (answer != null && answer.Value)
            //{

            //}


            //int x = null;
            //int? y = null;
            //Console.WriteLine(x);
            //Console.WriteLine(y);


            //Tuple<int, string> a = new Tuple<int, string>(1, "two");
            //(int, string, bool) x = (5, "text", true);
            //Console.WriteLine(x.Item3);



            //var words = new Dictionary<string, Dictionary<string, string>>();
            //words["az"] = new Dictionary<string, string>();
            //words["ru"] = new Dictionary<string, string>();
            //words["az"]["one"] = "bir";
            //words["az"]["two"] = "iki";
            //words["ru"]["one"] = "odin";
            //words["ru"]["two"] = "dva";
            //Console.WriteLine(words["ru"]["one"]);




            //var words = new Dictionary<string, string>()
            //{
            //    { "one", "bir" },
            //    { "two", "iki" },
            //    { "three", "uc" }
            //};
            //Console.WriteLine(words["two"]);



            //var words = new Dictionary<string, string>();
            //words.Add("one", "bir");
            //words["two"] = "iki";
            //words.Add("three", "uc");
            //Console.WriteLine(words["three"]);



            //Person one = new Person { Name = "One", Age = 1, Gender = Gender.Male };
            //Person two = new Person { Name = "One", Age = 1, Gender = Gender.Male };
            //Console.WriteLine(one.GetHashCode());
            //Console.WriteLine(two.GetHashCode());

            //Dictionary<Person, string> people = new Dictionary<Person, string>();
            //Person p1 = new Person { Name = "One", Age = 1, Gender = Gender.Male };
            //Person p2 = new Person { Name = "Two", Age = 2, Gender = Gender.Female };
            //Person p3 = new Person { Name = "Two", Age = 2, Gender = Gender.Female };
            //people.Add(p1, "One");
            //people.Add(p2, "Two");
            //people.Add(p3, "Three");
            //Console.WriteLine(people[p3]);



            //Dictionary<int, Person> people = new Dictionary<int, Person>();
            //people.Add(1, new Person { Name = "One", Age = 1, Gender = Gender.Male });
            //people.Add(2, new Person { Name = "Two", Age = 2, Gender = Gender.Female });
            //Console.WriteLine(people[2]);


            //string text = "Hello!";
            //int num = 42;
            //Money money = new Money { Dollars = 1, Cents = 1 };
            //object obj = new object();

            //Console.WriteLine(text.GetHashCode());
            //Console.WriteLine(num.GetHashCode());
            //Console.WriteLine(money.GetHashCode());
            //Console.WriteLine(obj.GetHashCode());




            //Person person1 = new Person { Name = "One", Age = 1, Gender = Gender.Male };
            //Person person2 = new Person { Name = "One", Age = 1, Gender = Gender.Male };
            //Money money = new Money { Dollars = 1, Cents = 1 };

            //if (person1.Equals(person2))
            //{
            //    Console.WriteLine("Yes");
            //}
            //else
            //{
            //    Console.WriteLine("No");
            //}



            //Money money = new Money { Dollars = 0, Cents = 0 };
            //if (money)
            //{
            //    Console.WriteLine("YES");
            //}
            //else
            //{
            //    Console.WriteLine("NO");
            //}



            //Money money = new Money { Dollars = 1, Cents = 99 };
            //Console.WriteLine(money);
            //Console.WriteLine(++money);
            //Console.WriteLine(money);



            //Money money1 = new Money { Dollars = 100, Cents = 50 };
            //Money money2 = new Money { Dollars = 100, Cents = 50 };

            //if (money1 == money2)
            //{
            //    Console.WriteLine("YES");
            //}
            //else
            //{
            //    Console.WriteLine("NO");
            //}



            //Person person1 = new Person { Name = "One", Age = 1, Gender = Gender.Male };
            //Person person2 = new Person { Name = "One", Age = 1, Gender = Gender.Male };

            //if (person1 == person2)
            //{
            //    Console.WriteLine("Yes");
            //}
            //else
            //{
            //    Console.WriteLine("No");
            //}
        }
    }
}