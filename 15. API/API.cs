//using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Xml.Serialization;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            //dynamic a = 10;
            //Console.WriteLine(a);
            //a = "Hello!";
            //Console.WriteLine(a.ToUpper());
            //a = null;
            //a = 0;
            //a = new List<double>() { 1.2, 3.5 };




            //WebClient webClient = new WebClient();
            //var url = "https://api.exchangeratesapi.io/latest?base=USD";
            //var json = webClient.DownloadString(url);

            //dynamic result = JsonConvert.DeserializeObject(json);
            //Console.WriteLine(result.rates.RUB);
            //Console.WriteLine(result.@base);
            //Console.WriteLine(result["rates"]["RUB"]);

            //decimal rubRate = result.rates.RUB;
            //Console.WriteLine(1000 / rubRate);




            //WebClient webClient = new WebClient();
            //var url = "https://www.omdbapi.com/?apikey=2c9d65d5&s=Matrix";
            //var json = webClient.DownloadString(url);

            //try
            //{
            //    dynamic result = JsonConvert.DeserializeObject(json);
            //    for (int i = 0; i < 11; i++)
            //    {
            //        Console.WriteLine(result.Search[i].Title);
            //        Console.WriteLine(result.Search[i].Year);
            //        Console.WriteLine();
            //    }
            //}
            //catch (Exception ex)
            //{
            //    Console.WriteLine(ex.Message);
            //}




            //WebClient webClient = new WebClient();
            //var url = "https://api.exchangeratesapi.io/latest?base=USD";
            //var json = webClient.DownloadString(url);

            //var result = JsonSerializer.Deserialize<ExchangeRates>(json);
            //Console.WriteLine(result.date);
            //Console.WriteLine(result.rates.RUB);




            //WebClient webClient = new WebClient();
            //var url = "https://api.exchangeratesapi.io/latest?base=USD";
            //var json = webClient.DownloadString(url);

            //var result = JsonSerializer.Deserialize<ExchangeRates>(json);
            //Console.WriteLine(result.date);
            //Console.WriteLine(result.rates.RUB);





            //WebClient webClient = new WebClient();
            //var url = "https://www.omdbapi.com/?apikey=2c9d65d5&s=Matrix";
            //var json = webClient.DownloadString(url);

            //var result = JsonSerializer.Deserialize<MovieApiResponse>(json);
            //Console.WriteLine($"Results: {result.totalResults}");
            //foreach (var item in result.Search)
            //{
            //    Console.WriteLine(item.Title);
            //    Console.WriteLine(item.Year);
            //    Console.WriteLine();
            //}


            
        }
    }

    //class ExchangeRates
    //{
    //    public Rates rates { get; set; }
    //    public string @base { get; set; }
    //    public DateTime date { get; set; }
    //}

    //class Rates
    //{
    //    public decimal RUB { get; set; }
    //    public decimal EUR { get; set; }
    //    public decimal USD { get; set; }
    //}


    public class ExchangeRates
    {
        public Rates rates { get; set; }
        public string @base { get; set; }
        public DateTime date { get; set; }
    }

    public class Rates
    {
        public decimal CAD { get; set; }
        public decimal HKD { get; set; }
        public decimal ISK { get; set; }
        public decimal PHP { get; set; }
        public decimal DKK { get; set; }
        public decimal HUF { get; set; }
        public decimal CZK { get; set; }
        public decimal GBP { get; set; }
        public decimal RON { get; set; }
        public decimal SEK { get; set; }
        public decimal IDR { get; set; }
        public decimal INR { get; set; }
        public decimal BRL { get; set; }
        public decimal RUB { get; set; }
        public decimal HRK { get; set; }
        public decimal JPY { get; set; }
        public decimal THB { get; set; }
        public decimal CHF { get; set; }
        public decimal EUR { get; set; }
        public decimal MYR { get; set; }
        public decimal BGN { get; set; }
        public decimal TRY { get; set; }
        public decimal CNY { get; set; }
        public decimal NOK { get; set; }
        public decimal NZD { get; set; }
        public decimal ZAR { get; set; }
        public decimal USD { get; set; }
        public decimal MXN { get; set; }
        public decimal SGD { get; set; }
        public decimal AUD { get; set; }
        public decimal ILS { get; set; }
        public decimal KRW { get; set; }
        public decimal PLN { get; set; }
    }


    public class MovieApiResponse
    {
        public Movie[] Search { get; set; }
        public string totalResults { get; set; }
        public string Response { get; set; }
    }

    public class Movie
    {
        public string Title { get; set; }
        public string Year { get; set; }
        public string imdbID { get; set; }
        public string Type { get; set; }
        public string Poster { get; set; }
    }

}
