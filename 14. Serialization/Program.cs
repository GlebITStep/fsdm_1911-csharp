using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Xml.Serialization;

namespace ConsoleApp2
{
    [Serializable] //Attribute
    public class Person
    {
        public Person()
        {

        }

        public Person(string name, string surname, int age)
        {
            Name = name;
            Surname = surname;
            Age = age;
        }

        public string Name { get; set; }
        public string Surname { get; set; }
        public int Age { get; set; }

        public override string ToString() => $"{Name} {Surname} {Age}";
    }

    class Program
    {
        static void Main(string[] args)
        {
            //Binary, XML, SOAP, JSON


            ////8 - JSON Read
            //string json = File.ReadAllText("people.json");
            //List<Person> people = JsonSerializer.Deserialize<List<Person>>(json);
            //foreach (var item in people)
            //{
            //    Console.WriteLine(item);
            //}


            ////7 - JSON Write
            //List<Person> people = new List<Person>()
            //{
            //    new Person("Nigar", "Khasaeva", 18),
            //    new Person("Gleb", "Skripnikov", 25),
            //    new Person("Sabik", "Mehdiev", 20),
            //};
            //string json = JsonSerializer.Serialize(people);
            //File.WriteAllText("people.json", json);
            ////File.WriteAllText("people.json", JsonSerializer.Serialize(people));


            ////6 - XML Read
            //List<Person> people;
            //XmlSerializer serializer = new XmlSerializer(typeof(List<Person>));
            //using (var fs = new FileStream("people.xml", FileMode.Open))
            //{
            //    people = serializer.Deserialize(fs) as List<Person>;
            //}
            //foreach (var item in people)
            //{
            //    Console.WriteLine(item);
            //}



            ////5 - XML Write
            //List<Person> people = new List<Person>()
            //{
            //    new Person("Nigar", "Khasaeva", 18),
            //    new Person("Gleb", "Skripnikov", 25),
            //    new Person("Sabik", "Mehdiev", 20),
            //};
            //XmlSerializer serializer = new XmlSerializer(typeof(List<Person>));
            //using (var fs = new FileStream("people.xml", FileMode.Create))
            //{
            //    serializer.Serialize(fs, people);
            //}


            ////4 - Binary read list
            //List<Person> people;
            //BinaryFormatter formatter = new BinaryFormatter();
            //using (var fs = new FileStream("people.bin", FileMode.Open))
            //{
            //    people = formatter.Deserialize(fs) as List<Person>;
            //}
            //foreach (var item in people)
            //{
            //    Console.WriteLine(item);
            //}


            ////3 - Binary write list
            //List<Person> people = new List<Person>()
            //{
            //    new Person("Nigar", "Khasaeva", 18),
            //    new Person("Gleb", "Skripnikov", 25),
            //    new Person("Sabik", "Mehdiev", 20),
            //};
            //BinaryFormatter formatter = new BinaryFormatter();
            //using (var fs = new FileStream("people.bin", FileMode.Create))
            //{
            //    formatter.Serialize(fs, people);
            //}


            //2 - Binary Read
            //Person person;
            //BinaryFormatter formatter = new BinaryFormatter();
            //using (var fs = new FileStream("person.bin", FileMode.Open))
            //{
            //    person = formatter.Deserialize(fs) as Person;
            //}
            //Console.WriteLine(person);


            ////1 - Binary Write
            //var person = new Person("Gleb", "Skripnikov", 25);            
            //BinaryFormatter formatter = new BinaryFormatter();
            //using (var fs = new FileStream("person.bin", FileMode.Create))
            //{
            //    formatter.Serialize(fs, person);
            //}




            //List<Person> people = new List<Person>()
            //{
            //    new Person("Nigar", "Khasaeva", 18),
            //    new Person("Gleb", "Skripnikov", 25),
            //    new Person("Sabik", "Mehdiev", 20),
            //};


            //using (FileStream fileStream = new FileStream("log.txt", FileMode.Create))
            //{
            //    using (StreamWriter streamWriter = new StreamWriter(fileStream))
            //    {
            //        for (int i = 0; i < 10; i++)
            //        {
            //            Thread.Sleep(1000);
            //            streamWriter.WriteLine(DateTime.Now);
            //            Console.WriteLine(DateTime.Now);
            //        }
            //    }
            //}

            //using FileStream fileStream = new FileStream("text.txt", FileMode.Create);
            //using StreamWriter streamWriter = new StreamWriter(fileStream);
            //foreach (var item in people)
            //{
            //    streamWriter.WriteLine(item);
            //}

            //using (FileStream fileStream = new FileStream("text.txt", FileMode.Open))
            //{
            //    using (StreamReader streamReader = new StreamReader(fileStream))
            //    {
            //        while (!streamReader.EndOfStream)
            //        {
            //            Console.WriteLine(streamReader.ReadLine()); 
            //        }
            //    }
            //}

            //using (FileStream fileStream = new FileStream("text.txt", FileMode.Create))
            //{
            //    using (StreamWriter streamWriter = new StreamWriter(fileStream))
            //    {
            //        foreach (var item in people)
            //        {
            //            streamWriter.WriteLine(item);
            //        }
            //    }
            //}

            //using (FileStream fileStream = new FileStream("text.txt", FileMode.Open))
            //{
            //    byte[] buffer = new byte[20];
            //    int count;
            //    while ((count = fileStream.Read(buffer, 0, 20)) != 0)
            //    {
            //        Console.WriteLine(Encoding.UTF8.GetString(buffer));
            //    }
            //}

            //using (FileStream fileStream = new FileStream("text.txt", FileMode.Create))
            //{
            //    foreach (var item in people)
            //    {
            //        byte[] bytes = Encoding.UTF8.GetBytes(item.ToString() + "\n");
            //        fileStream.Write(bytes, 0, bytes.Length);
            //    }
            //}
        }
    }
}
