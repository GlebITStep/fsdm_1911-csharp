using System;
using System.Collections.Generic;

namespace NewConsoleApp
{
    class Person
    {
        private int age;

        public Person(string name = "Empty", string surname = "Empty", int age = 0)
        {
            Name = name;
            Surname = surname;
            Age = age;
        }

        public override string ToString() => $"{Name} {Surname} {Age}";

        public string Name { get; set; }

        public string Surname { get; set; }

        public int Age
        {
            get { return age; }
            set
            {
                if (value > 0 && value <= 100)
                    age = value;
                else
                    age = 0;
            }
        }

    }

    class Program
    {
        static public int SumAll(params int[] numbers)
        {
            int result = 0;
            foreach (var item in numbers)
            {
                result += item;
            }
            return result;
        }

        static void Main(string[] args)
        {
            //float pi = 3.14f;
            //double num = pi;
            //decimal money = 13.4m;




            //SumAll(11, 22, 33, 44, 55);



            //List<Person> people = new List<Person>()
            //{
            //    new Person { Name = "Gleb", Surname = "Skripnikov", Age = 25 },
            //    new Person { Name = "One", Surname = "Two", Age = 3 },
            //    new Person { Name = "Four", Surname = "Five", Age = 6 },
            //};

            //foreach (var item in people)
            //{
            //    Console.WriteLine(item);
            //}



            ////dynamic array
            //List<int> list = new List<int>() { 11, 22, 33 };
            //list.Add(44);
            //list.Add(55);
            //list.AddRange(new int[]{ 66, 77, 88, 99});

            //list.Insert(5, 50);
            //list.InsertRange(3, new int[] { 10, 20, 30 });
            //Console.WriteLine(list.IndexOf(10));
            //Console.WriteLine(list.LastIndexOf(66));
            //Console.WriteLine();
            //list.Remove(10);
            //list.RemoveAt(4);
            //list.RemoveRange(5, 2);
            //list.Sort();

            //foreach (var item in list)
            //{
            //    Console.WriteLine(item);
            //}





            //Person person = new Person 
            //{ 
            //    Name = "Gleb", 
            //    Surname = "Skripnikov", 
            //    Age = 25 
            //};

            //Person person2 = new Person(surname:"One");

            //Console.WriteLine(person);
            //Console.WriteLine(person2);
        }
    }
}