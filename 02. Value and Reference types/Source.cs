using System;

namespace NewConsoleApp
{
    struct Point
    {
        public int x;
        public int y;
    }

    class Person
    {
        public int age;
        public string name;
    }

    class Program
    {
        //static void Func(ref Point p)
        //{
        //    p.x = 10;
        //    p.y = 10;
        //}

        static void Func(out Point p)
        {
            p.x = 10;
            p.y = 10;
        }

        static void Func(Person p)
        {
            p.age = 26;
            p.name = "Qleb";
        }

        static void Test(out int x)
        {
            x = 1;
        }

        static void MinMax(int[] arr, out int min, out int max)
        {
            //Algorithm
            min = 1;
            max = 9;
        }

        static void Main(string[] args)
        {
            //int[] arr = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            //MinMax(arr, out int min, out int max);
            //Console.WriteLine($"{min} - {max}");





            //int[] arr = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            //int min, max;
            //MinMax(arr, out min, out max);
            //Console.WriteLine($"{min} - {max}");



            //int number;
            //Test(out number);
            //Console.WriteLine(number);


            //Point point = new Point();
            //point.x = 5;
            //point.y = 6;
            ////Console.WriteLine($"X: {point.x}\tY: {point.y}");

            //Person person = new Person();
            //person.age = 25;
            //person.name = "Gleb";
            ////Console.WriteLine($"Name: {person.name}");
            ////Console.WriteLine($"Age: {person.age}");

            ////Func(ref point);
            ////Console.WriteLine($"X: {point.x}\tY: {point.y}");

            //Func(out point);
            //Console.WriteLine($"X: {point.x}\tY: {point.y}");

            //Func(person);
            //Console.WriteLine($"Name: {person.name}");
            //Console.WriteLine($"Age: {person.age}");



            ////String str = new String("Hello!");
            //string str = "Hello!";


            //VALUE TYPES
            //byte  -   Byte
            //short -   Int16
            //int   -   Int32
            //long  -   Int64
            //char  -   Char
            //bool  -   Boolean;
            //float -   Float
            //double  - Double
            //decimal - Decimal



            //REFERENCE TYPE
            //string  - String



            // Value type       - Stack | Struct
            // Reference type   - Heap  | Class



            Console.Write("Enter your age: ");
            //int age = Convert.ToInt32(Console.ReadLine());
            //int age = int.Parse(Console.ReadLine());
            int.TryParse(Console.ReadLine(), out int age);
            Console.WriteLine($"Age: {age}");



            //Console.Write("Enter your name: ");
            //string name = Console.ReadLine();
            //Console.WriteLine($"Name: {name}");



            //string name = "Gleb";
            //int age = 25;
            //Console.WriteLine($"Name: {name}");
            //Console.WriteLine($"Age: {age}");
        }
    }
}




// .NET
// Languages: C#, VB.NET, F#
// Desktop: WindowsFroms, WPF, UWP
// Mobile: Xamarin
// WEB Back-End: ASP.NET
// WEB Front-End: Blazor
// Games: Unity

// IL (MSIL) - Intermediate language
// CLR - Common Language Runtime
//      JIT Compiler - Just-In-Time Compiler
//      GC - Garbage Collector
// BCL - Base Class Library

// .NET Framework 4.8 - Only Windows
// .NET Core 3.0 - Cross-Platfotm, Open-Source
// .NET 5

// Reflector
// Dotfuscator