using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Net;
using System.Security;
using System.Text;
using System.Threading;
using Zoo;

namespace NewConsoleApp
{
    class Account<T>
    {
        public T Id { get; set; }
        public string Username { get; set; }

        public Account()
        {
            Id = default;
            Username = "Empty";
        }

        public Account(T id, string username)
        {
            Id = id;
            Username = username;
        }
    }

    class MyList<T> : IEnumerable    
    {
        class MyListEnumerator : IEnumerator
        {
            private T[] array;
            private int index = -1;

            public MyListEnumerator(T[] arr)
            {
                this.array = arr;   
            }

            public object Current => array[index];

            public bool MoveNext()
            {
                if (index < array.Length - 1)
                {
                    index++;
                    return true;
                }
                return false; 
            }

            public void Reset()
            {
                index = -1;
            }
        }

        private T[] array;

        public MyList(params T[] items)
        {
            array = items;
        }

        public int Length { get => array.Length; }

        public T this[int index]
        {
            get => array[index];
            set => array[index] = value;
        }

        public IEnumerator GetEnumerator()
        {
            //return new MyListEnumerator(array);

            //for (int i = array.Length - 1; i >= 0; i--)
            //{
            //    yield return array[i];
            //}

            for (int i = 0; i < array.Length; i++)
            {
                yield return array[i];
            }
            yield break;
        }
    }

    //Generics
    class Program
    {
        //static void Print<T>(T obj)
        //{
        //    Console.WriteLine(obj);
        //}

        //static void Print<TOne, TTwo>(TOne obj, TTwo obj2)
        //{
        //    Console.WriteLine(obj);
        //    Console.WriteLine(obj2);
        //}

        //static IEnumerable<int> GetNumber()
        //{
        //    yield return 1;
        //    yield return 2;
        //    yield return 3;
        //    yield return 4;
        //    yield return 5;
        //    yield break;
        //}

        //struct
        //class
        //new()
        static void Print<T>(T obj) where T : class, new()
        {
            Console.WriteLine(obj);
        }

        static void Main(string[] args)
        {
            Print(5);
            Print("Hello!");
            Print(new Account<string>());

            //foreach (var item in GetNumber())
            //{
            //    Console.WriteLine(item);
            //}


            //MyList<int> myList = new MyList<int>(11, 22, 33, 44, 55, 66);

            ////for (int i = 0; i < myList.Length; i++)
            ////{
            ////    Console.WriteLine(myList[i]);
            ////}

            //foreach (var item in myList)
            //{
            //    Console.WriteLine(item);
            //}

            ////var enumerator = myList.GetEnumerator();
            ////while (enumerator.MoveNext())
            ////{
            ////    var item = enumerator.Current;
            ////    Console.WriteLine(item);
            ////}
            ////enumerator.Reset();



            ////GUID
            //var guid = Guid.NewGuid();
            //Console.WriteLine(guid);


            //var account1 = new Account<Guid>(Guid.NewGuid(), "Gleb");
            //var account2 = new Account<Guid>(Guid.NewGuid(), "Sabik");
            //var account3 = new Account<Guid>(Guid.NewGuid(), "Nigar");



            //Account<int> account1 = new Account<int>(1, "Gleb");
            //Account<int> account2 = new Account<int>(2, "Sabik");
            //Account<int> account3 = new Account<int>(3, "Nigar");



            //Account<int> account1 = new Account<int>(1, "Gleb");
            //Account<string> account2 = new Account<string>("qwerty", "Gleb");




            //int num = 12;
            //Print(num);

            //decimal money = 15.5M;
            //Print(money);

            //string str = "Hello!";
            //Print(str);

            //Person p = new Person();
            //Print(p);




            //DateTime start = DateTime.Now;
            //for (int i = 0; i < 10000000; i++)
            //{
            //    Print(i);
            //}
            //Console.WriteLine(DateTime.Now - start);

            //Console.WriteLine();

            //start = DateTime.Now;
            //for (int i = 0; i < 10000000; i++)
            //{
            //    ObjPrint(i);
            //}
            //Console.WriteLine(DateTime.Now - start);
        }
    }
}