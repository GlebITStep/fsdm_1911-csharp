using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            //email
            Regex regex = new Regex(@"^[a-z0-9\-._]+@[a-z0-9-.]+\.[a-z]{2,10}$", RegexOptions.IgnoreCase);
            string text = "sk-ri_pn.ik.ov@itStep.org";
            Match match = regex.Match(text);
            if (match.Success)
                Console.WriteLine("YES");
            else
                Console.WriteLine("NO");


            ////[A-Z][a-z]
            //Regex regex = new Regex("^[A-Z][a-z]{1,50} [A-Z][a-z]{1,50}$");
            //string text = "Gleb Skripnikov";
            //Match match = regex.Match(text);
            //if (match.Success)
            //    Console.WriteLine("YES");
            //else
            //    Console.WriteLine("NO");



            ////{4} //{2,4} //{2,}
            //Regex regex = new Regex("^[0-9]{4} [0-9]{4} [0-9]{4} [0-9]{4}$");
            //string text = "1234 5678 1234 5678";
            //Match match = regex.Match(text);
            //if (match.Success)
            //    Console.WriteLine("YES");
            //else
            //    Console.WriteLine("NO");



            ////[]*   //* 0 - ... //+ 1 - ... //? 0 - 1
            //Regex regex = new Regex("^[0-9]?$");
            //string text = "5";
            //Match match = regex.Match(text);
            //if (match.Success)
            //    Console.WriteLine("YES");
            //else
            //    Console.WriteLine("NO");



            ////.
            //Regex regex = new Regex("^....$");
            //string text = "qwer";
            //Match match = regex.Match(text);
            //if (match.Success)
            //    Console.WriteLine("YES");
            //else
            //    Console.WriteLine("NO");



            ////^ and $
            //Regex regex = new Regex("^Gleb$");
            //string text = "Gleb";
            //Match match = regex.Match(text);
            //if (match.Success)
            //    Console.WriteLine("YES");
            //else
            //    Console.WriteLine("NO");



            ////$
            //Regex regex = new Regex("Gleb$");
            //string text = "My name is Gleb!";
            //Match match = regex.Match(text);
            //if (match.Success)
            //    Console.WriteLine("YES");
            //else
            //    Console.WriteLine("NO");



            ////^
            //Regex regex = new Regex("^Gleb");
            //string text = "Gleb is my name!";
            //Match match = regex.Match(text);
            //if (match.Success)
            //    Console.WriteLine("YES");
            //else
            //    Console.WriteLine("NO");



            //Regex regex = new Regex("Gleb");
            //string text = "My name is Gleb!";
            //Match match = regex.Match(text);
            //if (match.Success)
            //    Console.WriteLine("YES");
            //else
            //    Console.WriteLine("NO");




            ////Gleb
            //Console.WriteLine("Enter your name:");
            //string name = Console.ReadLine();

            ////email@domain.com
            //Console.WriteLine("Enter your email:");
            //string email = Console.ReadLine();






            //Dynamic array
            ////Generic
            //List<int> list;

            ////Non-generic
            //ArrayList arrayList = new ArrayList();
            //arrayList.Add(11);
            //arrayList.Add("Hello");
            //arrayList.Add(true);
            //foreach (var item in arrayList)
            //{
            //    Console.WriteLine(item);
            //}


            ////Generic collections
            //List<int> a;                        //dynamic array
            //LinkedList<int> b;                  //double linked list 
            //Queue<int> c;                       //queue
            //Stack<int> d;                       //stack
            //ObservableCollection<int> e;        //dynamic array
            //Dictionary<int, string> f;          //hash table
            //HashSet<int> g;                     //hash table
            //SortedList<int, string> k;          //binary tree
            //SortedSet<int> j;                   //binary tree
            //SortedDictionary<int, string> r;    //binary tree


            //g = new HashSet<int>();
            //g.Add(11);
            //g.Add(22);
            //g.Add(33);
            //g.Add(11);
            //foreach (var item in g)
            //{
            //    Console.WriteLine(item);
            //}


            //f = new Dictionary<int, string>();
            //f.Add(1, "One");
            //f.Add(2, "Two");
            //f.Add(3, "Three");
            //foreach (var item in f)
            //{
            //    Console.WriteLine(item);
            //}
        }
    }
}
