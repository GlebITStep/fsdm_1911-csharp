using System;
using System.Collections.Generic;
using System.IO;
using System.Security;
using System.Text;
using System.Threading;
using Zoo;

namespace NewConsoleApp
{
    enum Gender
    {
        Male = 1,
        Female = 2,
        Other = 0
    }

    class Person
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public Gender Gender { get; set; }

        public override bool Equals(object obj)
        {
            if (obj is Person person)
                return this.Name == person.Name && this.Age == person.Age;            
            return false;
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode() + Age.GetHashCode() + Gender.GetHashCode();
        }

        public override string ToString() => $@"{Name},{Age},{Gender}";
    }

    class Bird : Animal
    {
        public override void Say()
        {
            Console.WriteLine($"{Name} says Beee");
        }
    }

    struct Point
    {
        public int X { get; set; }
        public int Y { get; set; }
    }

    class Program
    {
        static void Print(int obj)
        {
            //Console.WriteLine(obj);
        }

        //static void Print(int obj)
        //{
        //    Console.WriteLine(obj);
        //}

        //static void Print(bool obj)
        //{
        //    Console.WriteLine(obj);
        //}

        //static void Print(Point obj)
        //{
        //    Console.WriteLine(obj);
        //}


        static void Main(string[] args)
        {
            DateTime start = DateTime.Now;
            for (int i = 0; i < 100000000; i++)
            {
                Print(i);
            }
            Console.WriteLine(DateTime.Now - start);


            //Cat cat = new Cat("Barsik", 3, 9);
            //Point point = new Point { X = 5, Y = 6 };
            //Print(cat);
            //Print(point);
            //Print(5);
            //Print(true);
            //Print("Hello!");



            ////boxing and unboxing
            //int num = 5;                        // value-type
            //Cat cat = new Cat("Barsik", 3, 9);  //reference-type

            //object obj = null;

            //obj = cat;
            //Console.WriteLine(obj);
            //(obj as Cat).Say();

            //obj = num;                  //boxing
            //Console.WriteLine(obj);
            //int result = (int)obj * 2;  //unboxing
            //Console.WriteLine(result);

            //int temp = num;             //no boxing
            //Console.WriteLine(obj);
            //int result2 = temp * 2;     //no unboxing
            //Console.WriteLine(result2);



            //Data data = new Data();



            //Animal animal = new Cat { Name = "Barsik", Age = 3, Lives = 9 };
            //Animal animal = new Dog("Barsik", 3, 9);
            //animal.Say();
            //Console.WriteLine();
            //animal.Info();
            //Console.WriteLine();
            //Console.WriteLine(animal);


            //Animal animal = new Animal { Name = "Empty", Age = 0 };
            //animal.Say();
            //Cat cat = new Cat { Name = "Barsik", Age = 3 };
            //cat.Say();

            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }
    }
}