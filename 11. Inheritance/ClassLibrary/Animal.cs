﻿using System;

namespace Zoo
{
    public class Data
    {
        public int a = 1;
        protected int b = 2;
        private int c = 3;
        internal int d = 4;
        protected internal int e = 5;
        private protected int f = 5;
    }

    public class Data2 : Data
    {
        public void Foo()
        {
            //this.f
        }
    }

    class Test
    {
        public void Foo()
        {
            Data data = new Data();
            
        }
    }

    //internal / public
    public abstract class Animal
    {
        protected Animal(string name = "Empty", int age = 0)
        {
            Name = name;
            Age = age;
        }

        public string Name { get; set; }
        public int Age { get; set; }

        public abstract void Say();

        public virtual void Info()
        {
            Console.WriteLine($"Name: {Name}");
            Console.WriteLine($"Age: {Age}");
        }

        public override string ToString() => $"{Name} {Age}";
    }
}
