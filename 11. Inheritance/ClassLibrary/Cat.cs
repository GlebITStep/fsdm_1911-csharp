﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zoo
{
    public class Cat : Animal
    {
        public Cat(string name, int age, int lives) : base(name, age)
        {
            Lives = lives;
        }

        public int Lives { get; set; }

        ///public new void Say()
        public override void Say()
        {
            Console.WriteLine($"{Name} says Meow");
        }

        public override void Info()
        {
            Console.WriteLine("Type: Cat");
            base.Info();
            Console.WriteLine($"Lives: {Lives}");
        }
    }
}
