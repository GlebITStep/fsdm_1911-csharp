﻿using System;

namespace Zoo
{
    public class Fish : Animal
    {
        public override void Say()
        {
            Console.WriteLine($"{Name} says BulBul");
        }
    }
}
