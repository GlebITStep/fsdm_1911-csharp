﻿using System;

namespace Zoo
{
    public class Dog : Animal
    {
        public double Weight { get; set; }

        public Dog(string name, int age, double weight) : base(name, age)
        {
            Weight = weight;
        }

        public override void Say()
        {
            Console.WriteLine($"{Name} says Woof");
        }

        public override void Info()
        {
            Console.WriteLine("Type: Dog");
            base.Info();
            Console.WriteLine($"Weight: {Weight}");
        }
    }
}
