using System;
using System.Collections.Generic;
using System.IO;
using System.Security;
using System.Text;
using System.Threading;

namespace NewConsoleApp
{
    class Program
    {
        class IntArray
        {
            private int[] arr = new int[10] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };

            public int Length { get; } = 10;

            //Indexer
            public int this[int index]
            {
                get { return arr[index]; }
                set { arr[index] = value; }
            }
        }

        class EnAzDictionary
        {
            //public string this[string word]
            //{
            //    get 
            //    {
            //        switch (word)
            //        {
            //            case "one":
            //                return "bir";
            //            case "two":
            //                return "iki";
            //            case "three":
            //                return "uc";
            //            default:
            //                return "Error!";
            //        }
            //    }
            //}

            public string this[string word]
            {
                get
                {
                    return word switch
                    {
                        "one" => "bir",
                        "two" => "iki",
                        "three" => "uc",
                        _ => "Error!"
                    };
                }
            }
        }

        enum Color : ulong
        {
            Red = 2345239745932745239,
            Green,
            Blue
        }

        enum Gender
        { 
            Male = 1,
            Female = 2,
            Other = 0
        }

        class Person
        {
            public string Name { get; set; }
            public int Age { get; set; }
            public Gender Gender { get; set; }

            public override string ToString() => $@"{Name} {Age} {Gender}";
        }

        struct Point
        {
            public int X { get; set; }
            public int Y { get; set; }

            public Point(int x, int y)
            {
                X = x;
                Y = y;
            }

            public override string ToString() => $"X: {X}; Y: {Y}";
        }

        static void Main(string[] args)
        {
            DateTime start = DateTime.Now;
            
            int result = 0;
            for (int i = 0; i < 100000; i++)
            {
                for (int j = 0; j < 100000; j++)
                {
                    result += i * j;
                }
            }

            DateTime finish = DateTime.Now;
            TimeSpan offset = finish - start;

            Console.WriteLine(offset);




            //Console.CursorVisible = false;
            //while (true)
            //{
            //    DateTime dateTime = DateTime.Now;
            //    Console.WriteLine(dateTime);
            //    Console.SetCursorPosition(0, 0);
            //}




            //Point point = new Point(10, 5);
            //Console.WriteLine(point);



            //string name = null;
            //string str = name ?? "Empty";


            //Person person = new Person();
            //Console.Write("Enter name: ");
            //person.Name = Console.ReadLine();
            //Console.Write("Enter age: ");
            //person.Age = int.Parse(Console.ReadLine());

            ////Console.Write("Enter gender (1 - Male, 2 - Female, 3 - Other): ");
            ////person.Gender = (Gender)int.Parse(Console.ReadLine());

            ////Console.Write("Enter gender (Male, Female, Other): ");
            ////person.Gender = (Gender)Enum.Parse(typeof(Gender), Console.ReadLine());

            //Console.Write("Enter gender (Male, Female, Other): ");
            //Enum.TryParse(typeof(Gender), Console.ReadLine(), out object result);
            //person.Gender = result == null ? Gender.Other : (Gender)result;

            //Console.WriteLine(person);




            //Person person = new Person
            //{
            //    Name = "Gleb",
            //    Age = 25,
            //    Gender = Gender.Male
            //};
            //Console.WriteLine(person);


            //Color background = Color.Blue;
            //int color = (int)background;
            //Console.WriteLine(background);


            //EnAzDictionary dictionary = new EnAzDictionary();
            //Console.WriteLine(dictionary["one"]);


            //IntArray intArray = new IntArray();
            //Console.WriteLine(intArray.Length);
            //intArray[5] = 99;
            //Console.WriteLine(intArray[5]);
        }
    }
}