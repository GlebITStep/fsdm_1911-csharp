using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Text.Json;
using Newtonsoft.Json;

namespace ConsoleApp2
{
    static class StringExtensions
    {
        public static void Print(this object obj, ConsoleColor color = ConsoleColor.Gray)
        {
            Console.ForegroundColor = color;
            Console.WriteLine(obj);
            Console.ForegroundColor = ConsoleColor.Gray;
        }

        //public static void Print(string str)
        //{
        //    Console.WriteLine(str);
        //}
    }

    public class Person : IComparable<Person>
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public DateTime BirthDate { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string Language { get; set; }
        public int Salary { get; set; }
        public string Job { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }

        public override string ToString()
        {
            return $"{Id} {FirstName} {LastName} {Gender} {BirthDate.ToShortDateString()} {Country} {City} {Job} {Salary}";
        }

        public int CompareTo(Person other)
        {
            return this.Salary - other.Salary;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            List<Person> people;
            var json = File.ReadAllText("data.json");
            //people = JsonSerializer.Deserialize<List<Person>>(json);
            people = JsonConvert.DeserializeObject<List<Person>>(json);


            //---------------------------------------------------


            ////Sum, Min, Max, Average, Count
            //var result = people.Min(delegate(Person x) { return x.Salary; });

            //var result0 = people.Min();
            //Console.WriteLine(result0);

            //var result = people.Min(x => x.BirthDate);
            //Console.WriteLine(result);

            //var result2 = people.Max(x => x.BirthDate);
            //Console.WriteLine(result2);

            //var result3 = people.Average(x => x.Salary);
            //Console.WriteLine(result3);

            //var result4 = people.Sum(x => x.Salary);
            //Console.WriteLine(result4);

            //var result5 = people.Count(x => x.Country == "Azerbaijan");
            //Console.WriteLine(result5);


            //---------------------------------------------------


            //Where, First, Last
            //var result = people
            //    .Where(x => x.Country == "United States")
            //    .Where(x => (DateTime.Now - x.BirthDate).TotalDays > 365 * 70)
            //    .Where(x => x.Salary > 5000);

            //foreach (var item in result)
            //{
            //    Console.WriteLine(item);
            //}


            //var result = people
            //    .Where(x => x.Country == "United States")
            //    .Where(x => (DateTime.Now - x.BirthDate).TotalDays > 365 * 70)
            //    .Where(x => x.Salary > 2000)
            //    .Average(x => x.Salary);

            //Console.WriteLine(result);


            ////First (throws Exception) or FirsOrDefault (returns null)
            //var result = people
            //    .FirstOrDefault(x => x.Country == "Azerbaijan" && x.Salary > 19000);
            //Console.WriteLine(result?.ToString() ?? "No results!");
            ////Console.WriteLine(result != null ? result.ToString() : "No results!");


            //Last (throws Exception) or LastOrDefault (returns null)
            //var result = people
            //    .Last(x => x.Country == "Azerbaijan");
            //Console.WriteLine(result);


            //---------------------------------------------------


            //All, Any, Contains
            //var result = people.All(x => x.Salary > 200);
            //Console.WriteLine(result);

            //var result = people.Any(x => x.Salary > 19999);
            //Console.WriteLine(result);

            //var person = people[500];
            //people.RemoveAt(500);
            //var result = people.Contains(person);
            //Console.WriteLine(result);


            //---------------------------------------------------


            ////OrderBy, OrderByDescending, ThenBy, ThenByDescending
            //var result = people
            //    .OrderByDescending(x => x.Country)
            //    .ThenByDescending(x => x.Salary);

            //foreach (var item in result)
            //{
            //    Console.WriteLine(item);
            //}


            //---------------------------------------------------


            ////Skip, Take
            //var result = people
            //    .Where(x => x.Country == "United States")
            //    .OrderByDescending(x => x.Salary)
            //    .Take(5);

            //foreach (var item in result)
            //{
            //    Console.WriteLine(item);
            //}


            //int page = 0;
            //while (true)
            //{
            //    var result = people
            //        .OrderByDescending(x => x.Salary)
            //        .Skip(page * 5)
            //        .Take(5);

            //    foreach (var item in result)
            //    {
            //        Console.WriteLine(item);
            //    }

            //    Console.WriteLine("Press any key to go to the next page...");
            //    Console.ReadKey();
            //    Console.Clear();

            //    page++;
            //}

            //var result = people
            //    .TakeLast(5);

            //foreach (var item in result)
            //{
            //    Console.WriteLine(item);
            //}


            //var result = people
            //    .TakeWhile(x => x.Country != "Germany");

            //foreach (var item in result)
            //{
            //    Console.WriteLine(item);
            //}


            //var result = people
            //    .SkipWhile(x => x.Salary < 19990);

            //foreach (var item in result)
            //{
            //    Console.WriteLine(item);
            //}





            //IEnumerable<int> nums = new List<int> { 43, 62, 11, 48, 95, 23, 56 };
            //var result = nums.OrderByDescending(x => x).Take(3);
            //foreach (var item in result)
            //{
            //    Console.WriteLine(item);
            //}




            ////Extension methods
            ////Linq

            //string text = "Hello!";
            ////StringExtensions.Print(text);
            //text.Print(ConsoleColor.Red);
            //text.Print(ConsoleColor.Green);

            //int num = 42;
            //num.Print(ConsoleColor.Blue);
        }
    }
}
