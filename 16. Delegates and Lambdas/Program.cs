//using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Xml.Serialization;

namespace ConsoleApp2
{
    delegate void Del(string num);

    class Program
    {
        static void Print(string text)
        {
            Console.WriteLine(text);
        }

        static void PrintRed(string text)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(text);
            Console.ForegroundColor = ConsoleColor.Gray;
        }

        static void PrintUpper(string text)
        {
            Console.WriteLine(text.ToUpper());
        }

        //static void FilterEvenNumbers(int[] arr)
        //{ 

        //}

        //static void FilterPosiveNumbers(int[] arr)
        //{

        //}

        //static void FilterNumbersMoreThan50(int[] arr)
        //{

        //}

        //delegate bool Condition(int item);

        static bool IsPositive(int item)
        {
            return item > 0;
        }

        static bool IsEven(int item)
        {
            return item % 2 == 0;
        }

        //static void Filter(int[] arr, Condition condition)
        //{
        //    foreach (var item in arr)
        //    {
        //        if (condition.Invoke(item))
        //        {
        //            Console.WriteLine(item);
        //        }
        //    }
        //}

        static bool MoreThan3Letters(string str)
        {
            return str.Length > 3;
        }

        //delegate bool Condition<T>(T item);

        //static void Filter<T>(T[] arr, Condition<T> condition)
        //{
        //    foreach (var item in arr)
        //    {
        //        if (condition.Invoke(item))
        //        {
        //            Console.WriteLine(item);
        //        }
        //    }
        //}

        static void Filter<T>(T[] arr, Predicate<T> pred)
        {
            if (pred == null)
                throw new ArgumentException("Predicate is null", "pred");

            foreach (var item in arr)
            {
                if (pred.Invoke(item))
                {
                    Console.WriteLine(item);
                }
            }
        }

        static bool IsMoreThan50(int item)
        {
            return item > 50;
        }

        static bool IsLessThan30(int item)
        {
            return item < 30;
        }

        //delegate TResult Test<TParam, TResult>(TParam param);

        //class Animal { } 
        //class Cat : Animal { }

        //static Animal Foo(Cat animal)
        //{
        //    return animal;
        //}

        //+
        //0
        //-
        static int Compare(int x, int y)
        {
            return x - y;

            //if (x > y)
            //    return 1;
            //else if (x < y)
            //    return -1;
            //else
            //    return 0;
        }

        static void Main(string[] args)
        {
            //List<int> list = new List<int> { 11, -22, -33, 44, 55, -66 };
            //list.Sort((x, y) => x - y);
            //foreach (var item in list)
            //{
            //    Console.WriteLine(item);
            //}



            //List<int> list = new List<int> { 11, 22, 33, 44, 55, 66 };
            //var result = list.ConvertAll(x => x * 2);
            //result.ForEach(x => Console.WriteLine(x));



            //List<int> list = new List<int> { 11, 22, 33, 44, 55, 66 };
            //List<string> result = list.ConvertAll(x => x.ToString());
            //foreach (var item in result)
            //{
            //    Console.WriteLine(item);
            //}


            //List<int> list = new List<int> { 11, 22, 33, 44, 55, 66 };
            //bool result = list.TrueForAll(x => x > 0);
            //if (result)
            //    Console.WriteLine("YES");
            //else
            //    Console.WriteLine("NO");



            //List<int> list = new List<int> { 11, 22, 33, 44, 55, 66 };
            //list.ForEach(x => Console.WriteLine(x));



            //List<int> list = new List<int> { 11, 22, 33, 44, 55, 66 };
            //int result = list.RemoveAll(x => x < 30);
            //Console.WriteLine($"Deleted: {result}");
            //foreach (var item in list)
            //{
            //    Console.WriteLine(item);
            //}


            //List<int> list = new List<int> { 11, 22, 33, 44, 55, 66 };
            //List<int> results = list.FindAll(x => x % 2 == 0);
            //foreach (var item in results)
            //{
            //    Console.WriteLine(item);
            //}


            //List<int> list = new List<int> { 11, 22, 33, 44, 55, 66 };
            //int result = list.Find(x => x > 50);
            //Console.WriteLine(result);



            //List<int> list = new List<int> { 11, 22, 33, 44, 55, 66 };
            ////bool result = list.Contains(55);
            //bool result = list.Exists(x => x > 50);
            //if (result)
            //    Console.WriteLine("YES");
            //else
            //    Console.WriteLine("NO");


            //Func<int> func1 = () => 5;
            //Func<int, int> func2 = x => 5;
            //Func<int, int, int> func3 = (x, y) => 5;

            //int[] arr = new int[] { 11, -22, -33, 44, 55, -66, 77, -88, 99 };
            //Filter(arr, x => x > 50);
            //Console.WriteLine();
            //Filter(arr, x => x < 30);
            //Console.WriteLine();
            //Filter(arr, x => x % 2 == 0);
            //Console.WriteLine();
            //Filter(arr, x => x > 0);

            //Console.WriteLine();
            //Filter(arr, x => 
            //{ 
            //    Console.WriteLine("TEST"); 
            //    return x > 0; 
            //});





            //int[] arr = new int[] { 11, -22, -33, 44, 55, -66, 77, -88, 99 };
            //Filter(arr, delegate (int item) { return item > 50; });
            //Console.WriteLine();
            //Filter(arr, delegate (int item) { return item < 30; });
            //Console.WriteLine();
            //Filter(arr, delegate (int item) { return item % 2 == 0; });
            //Console.WriteLine();
            //Filter(arr, delegate (int item) { return item > 0; });



            //Del del1 = PrintRed;

            //Del del2 = delegate (string text) { Console.WriteLine(text + "!!!!!"); };

            //Del del3 = x => Console.WriteLine("!!!!!" + x);

            //del1("Hello!");
            //del2("Hello!");
            //del3("Hello!");



            //int[] arr = new int[] { 11, -22, -33, 44, 55, -66, 77, -88, 99 };
            //Filter(arr, IsLessThan30);


            //Action a;
            //Action<int> b;
            //Predicate<int> c;
            //Func<int> d;
            //Func<int, int, int> e;

            //string[] strs = new string[] { "One", "Two", "Three" };
            //Filter(strs, MoreThan3Letters);
            //Filter(strs, null);

            //int[] arr = new int[] { 11, -22, -33, 44, 55, -66, 77, -88, 99 };
            //Filter(arr, IsPositive);

            //Filter(arr, IsEven);
            //Condition condition = IsPositive;
            //condition += IsEven;
            //Filter(arr, condition);

            //Del del = Print;
            //del += PrintRed;
            //del += PrintUpper;
            //del?.Invoke("Hello");

            //Console.WriteLine();

            //del -= Print;
            //del -= PrintRed;
            //del -= PrintUpper;
            //del?.Invoke("Hello");





            Del del = Print;
            del += PrintRed;
            del += PrintUpper;
            del += PrintRed;
            del("Hello!");

            Console.WriteLine();

            del -= PrintRed;
            del -= PrintRed;
            del -= PrintRed;
            del("Hello!");




            //Del del = Print;
            //del += PrintRed;
            //del += PrintUpper;
            //del("Hello!");





            //Del del1 = Print;
            //Del del2 = PrintRed;
            //del1("Hello!");
            //del2("Hello!");
        }
    }
}
