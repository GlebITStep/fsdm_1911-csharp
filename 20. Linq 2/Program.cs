using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Text.Json;
using Newtonsoft.Json;

namespace ConsoleApp2
{
    static class StringExtensions
    {
        public static void Print(this object obj, ConsoleColor color = ConsoleColor.Gray)
        {
            Console.ForegroundColor = color;
            Console.WriteLine(obj);
            Console.ForegroundColor = ConsoleColor.Gray;
        }

        //public static void Print(string str)
        //{
        //    Console.WriteLine(str);
        //}
    }

    public class Person : IComparable<Person>
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public DateTime BirthDate { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string Language { get; set; }
        public int Salary { get; set; }
        public string Job { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }

        public override string ToString()
        {
            return $"{Id} {FirstName} {LastName} {Gender} {BirthDate.ToShortDateString()} {Country} {City} {Job} {Salary}";
        }

        public string ToShortString()
        {
            return $"{Id} {FirstName} {LastName} {BirthDate.ToShortDateString()} {Country} {City}";
        }

        public int CompareTo(Person other)
        {
            return this.Salary - other.Salary;
        }

        // public int CompareTo(Person other)
        // {
        //     if (this.FirstName > other.FirstName) return 1;
        //     else if (this.FirstName < other.FirstName) return -1;
        //     else return 0;
        // }
    }


    class Student
    {
        public string FullName { get; set; }
        public int Average { get; set; }

        public override string ToString() => $"{FullName} {Average}";
    }

    class StudentNameComparer : IEqualityComparer<Student>
    {
        public bool Equals(Student x, Student y)
        {
            return x.FullName == y.FullName;
        }

        public int GetHashCode(Student obj)
        {
            return obj.FullName.GetHashCode() ^ obj.Average.GetHashCode();
        }
    }

    class StudentAverageComparer : IEqualityComparer<Student>
    {
        public bool Equals(Student x, Student y)
        {
            return x.Average == y.Average;
        }

        public int GetHashCode(Student obj)
        {
            return obj.Average;
        }
    }

    /*
        Penny Micallef	        3
        Hobart Garrit	        6
        Allan Brok	            7
        Katinka Matussow	    12
        Faina Plester	        10
        Saunderson Reucastle	2
        Merrill Samworth	    6
        Parsifal Quernel	    4
        Tilly Pantry	        8
        Martelle Kupka	        11
     */

    class Program
    {
        static void Main(string[] args)
        {
            List<Person> people;
            var json = File.ReadAllText("data.json");
            people = JsonConvert.DeserializeObject<List<Person>>(json);

            List<int> numbers = new List<int> { 3, 6, 7, 1, 4, 7, 6, 1 };
            List<int> numbers2 = new List<int> { 9, 3, 1, 5, 8, 2, 0 };

            List<Student> students = new List<Student>
            {
                new Student { Average = 3, FullName = "Penny Micallef"},
                new Student { Average = 3, FullName = "Penny Micallef"},
                new Student { Average = 6, FullName = "Hobart Garrit"},
                new Student { Average = 7, FullName = "Allan Brok"},
                new Student { Average = 12, FullName = "Katinka Matussow"},
                new Student { Average = 12, FullName = "Katinka Matussow"},
                new Student { Average = 10, FullName = "Faina Plester"},
                new Student { Average = 10, FullName = "Faina Plester"},
                new Student { Average = 2, FullName = "Saunderson Reucastle"},
                new Student { Average = 6, FullName = "Merrill Samworth"},
                new Student { Average = 4, FullName = "Parsifal Quernel"},
                new Student { Average = 3, FullName = "Penny Micallef"},
                new Student { Average = 8, FullName = "Tilly Pantry"},
                new Student { Average = 11, FullName = "Martelle Kupka"},
                new Student { Average = 11, FullName = "Martelle Kupka"},
                new Student { Average = 11, FullName = "Martelle Kupka"},
            };



            //var testResult = people
            //    .Where(x => x.Country == "Azerbaijan")
            //    .Select(x => new { Surname = x.LastName, x.City })
            //    .OrderBy(x => x.Surname);

            var result = from x in people
                         where x.Country == "Azerbaijan"
                         orderby x.City
                         select new { Surname = x.LastName, x.City };

            foreach (var item in result)
            {
                Console.WriteLine(item);
            }






            //var s1 = new Student { Average = 10, FullName = "Faina Plester" };
            //var s2 = new Student { Average = 10, FullName = "Faina Plester" };
            //Console.WriteLine(s1.GetHashCode());
            //Console.WriteLine(s2.GetHashCode());

            //if (s1.Equals(s2))
            //    Console.WriteLine("YES");
            //else
            //    Console.WriteLine("NO");





            //var result = students.Distinct();
            //Console.WriteLine($"Count: {result.Count()}");
            //foreach (var item in result)
            //{
            //    Console.WriteLine(item);
            //}




            //var result = numbers.Except(numbers2);
            //foreach (var item in result)
            //{
            //    Console.WriteLine(item);
            //}



            //var result = numbers.Intersect(numbers2);
            //foreach (var item in result)
            //{
            //    Console.WriteLine(item);
            //}



            //var result = numbers.Union(numbers2);
            //foreach (var item in result)
            //{
            //    Console.WriteLine(item);
            //}



            //var result = numbers.Concat(numbers2);
            //foreach (var item in result)
            //{
            //    Console.WriteLine(item);
            //}



            //var result = numbers.Distinct();
            //foreach (var item in result)
            //{
            //    Console.WriteLine(item);
            //}



            //var result = people.ToLookup(x => x.FirstName[0], x => $"{x.FirstName} {x.LastName}");
            //foreach (var item in result['Z'])
            //{
            //    Console.WriteLine(item);
            //}




            //var result = people
            //    .GroupBy(x => x.Country)
            //    .Select(x => new { Country = x.Key, AvgSalary = x.Average(y => y.Salary) })
            //    .OrderByDescending(x => x.AvgSalary);

            //foreach (var item in result)
            //{
            //    Console.WriteLine($"{item.AvgSalary} - {item.Country}");
            //}




            //var result = people
            //    .GroupBy(x => x.Country)
            //    .OrderBy(x => x.Key);

            //foreach (var group in result)
            //{
            //    Console.WriteLine(group.Key.ToUpper());
            //    foreach (var person in group.OrderBy(x => x.FirstName))
            //    {
            //        Console.WriteLine($"\t{person.ToShortString()}");
            //    }
            //    Console.WriteLine();
            //}



            //var result = people
            //    .Where(x => x.Country == "United States")
            //    .ToDictionary(x => x.Id);

            //Console.WriteLine(result[625]);

            ////foreach (var item in result)
            ////{
            ////    Console.WriteLine(item);
            ////}



            //var result = numbers.Select(x => x * 10);
            //foreach (var item in result)
            //{
            //    Console.WriteLine(item);
            //}



            //var result = people
            //    .Where(x => x.Country == "United States")
            //    .Select(x => new { Fullname = $"{x.FirstName} {x.LastName}", x.Salary });

            //foreach (var item in result)
            //{
            //    Console.WriteLine($"Fullname: {item.Fullname}\tSalary: {item.Salary}$");
            //}




            //var result = people
            //    .Where(x => x.Country == "United States")
            //    .Select(x => $"{x.FirstName} {x.LastName}");

            //foreach (var item in result)
            //{
            //    Console.WriteLine(item);
            //}




            //var result = people
            //    .Where(x => x.Country == "United States")
            //    .Select(x => x.LastName);

            //foreach (var item in result)
            //{
            //    Console.WriteLine(item);
            //}




            ////First(OrDefault), Last(OrDefault), Single(OrDefault) 
            //numbers.FirstOrDefault(x => x == 7);
            //numbers.LastOrDefault(x => x == 7);
            //numbers.SingleOrDefault(x => x == 7);



            ////Deferred execution
            //var result = people.Where(x => x.Country == "Azerbaijan");

            //foreach (var item in result)
            //{
            //    Console.WriteLine(item);
            //}

            //Console.WriteLine();
            //people.RemoveAt(618);

            //foreach (var item in result)
            //{
            //    Console.WriteLine(item);
            //}
        }
    }
}
