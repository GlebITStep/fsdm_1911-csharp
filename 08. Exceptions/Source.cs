using System;
using System.Collections.Generic;
using System.IO;
using System.Security;
using System.Text;
using System.Threading;

namespace NewConsoleApp
{
    class Program
    {
        static int[] arr = new int[] { 11, 22, 0, 33 };

        static int FindByIndex(int index)
        {
            return arr[index];
        }

        static double Division(double num1, double num2)
        {
            if (num2 == 0)
                throw new Exception("Division by zero!");

            return num1 / num2;
        }

        static void Main(string[] args)
        {
            unchecked
            {
                int x = int.MaxValue;
                Console.WriteLine(x);
                x++;
                Console.WriteLine(x);
            }

            


            //try
            //{
            //    //File.WriteAllText("con", "Hello!");
            //    //Open file
            //    //Write to file
            //}
            //catch (SecurityException)
            //{
            //    Console.WriteLine("You cannot create file!");
            //}
            //catch (DirectoryNotFoundException)
            //{
            //    Console.WriteLine("Gde papka?!");
            //}
            //catch (Exception)
            //{
            //    Console.WriteLine("Error!");
            //}
            //Console.WriteLine("End!");



            //try
            //{
            //    int num1 = FindByIndex(1);
            //    int num2 = FindByIndex(2);
            //    double result = Division(num1, num2);
            //    Console.WriteLine(result);
            //}
            //catch (Exception ex)
            //{
            //    Console.WriteLine(ex.Message);
            //}
            //Console.WriteLine("End!");
        }
    }
}